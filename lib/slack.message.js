const https = require("https");
const log = require("./log");

module.exports = async (message) => {
  if (process.env.SILENT_SLACK) {
    log.ok(`silent slack`, message.text);
    return;
  }
  const data = JSON.stringify(message);
  // console.log(data, `/services/${process.env.SLACK_HOOK}`);
  const options = {
    hostname: "hooks.slack.com",
    port: 443,
    // the channel is an env var to the hook url secret
    path: `/services/${process.env.SLACK_HOOK}`,
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "Content-Length": data.length,
    },
  };

  log.ok({ options }, data);

  return new Promise((resolve, reject) => {
    const req = https.request(options, (res) => {
      log.ok(`statusCode: ${res.statusCode}`);

      res.on("data", (d) => {
        console.log(d.toString());
        // process.stdout.write(d);
      });
      req.on("end", () => {
        resolve();
      });
    });

    req.on("error", (error) => {
      log.error(`http error`, error);
      reject(error);
    });

    req.write(data);
    req.end();
  });
};
