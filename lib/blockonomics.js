const http = require('https');
const agent = require('./agent');
const options = {
    headers: {
        'Origin': 'https://www.blockonomics.co',
        'User-Agent': agent(),
        'Accept': 'application/json, text/plain, */*',
        'Accept-Language': 'en-US,en;q=0.9',
        'Content-Type': 'application/json;charset=UTF-8',
        'Content-Length': 0,
        'Referer': 'https://www.blockonomics.co/',
        'Connection': 'keep-alive'
    },
    method: 'POST',
    host: 'www.blockonomics.co',
    path: '/api/balance'
};

const balance = module.exports = function (addr, cb) {
    // console.log(`fetching blockonomics balance for ${addr}`)
    const postData = '{"addr":"' + addr + '","secret":"Block@nomics"}';
    options.headers['User-Agent'] = agent();
    options.headers['Content-Length'] = postData.length;
    if (process.env.VERBOSE) console.log(`curl --data-binary '${postData}' https://${options.host + options.path}`)
    const req = http.request(options, function (res) {
        const { statusCode } = res;
        res.setEncoding('utf8');
        // Continuously update stream with data
        let body = '';
        res.on('data', function (d) {
            body += d;
        });
        res.on('end', function () {
            // console.log(statusCode, body);
            if (statusCode !== 200) {
                console.log(statusCode, body);
                return cb({ balance: '?', txn: '?', src: 'bl' });
            }
            try {
                cb(null, JSON.parse(body).response);
            } catch (e) {
                cb(e);
            }
        });
    });
    req.on('error', (e) => {
        console.error(e);
        return cb({ balance: '?', txn: '?', src: 'bl' });
    });

    req.write(postData, 'binary');
    req.end();

    return;
}