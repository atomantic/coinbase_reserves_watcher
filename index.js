const api = require("./lib/blockchain.info");
const fs = require("fs");
const retry = require("async.retry");
const series = require("async.series");
const chunk = require("lodash.chunk");

const state = require("./state.json");

const slack = require("./lib/slack.message");
const sleep = require("./lib/sleep");

// load up the addresses to watch
const walletList = fs.readFileSync("./lib/wallets.txt").toString();
const deadList = fs.readFileSync("./lib/dead.txt").toString();

const addresses = walletList.split("\n");

// api only allows 50 addresses at a time
const address_chunks = chunk(addresses, 50);

// accumulator
let total = 0;
let biggest = { balance: 0 };
let smallest = { balance: Infinity };

const badwallets = [];

const tasks = address_chunks.map((batch) => {
  return (cb) => {
    retry(
      {
        times: 5,
        interval: 5000,
      },
      (callback) => {
        api(batch.join(" "), function (error, result) {
          if (error) {
            console.error("error thrown in API call", error);
            return callback(error);
          }
          for (let i = 0; i < result.length; i++) {
            let res = result[i];
            console.log(`${res.addr} ${res.balance}`);
            if (!res.balance) {
              console.log("🚫 bad", res);
              badwallets.push(res.addr);
            }
            total += res.balance;
            if (biggest.balance < res.balance) biggest = res;
            if (smallest.balance > res.balance) smallest = res;
          }
          callback();
        });
      },
      cb
    );
  };
});

const readableBTC = (num) =>
  num.toLocaleString(undefined, {
    minimumFractionDigits: 8,
    maximumFractionDigits: 8,
  });

series(tasks, async (err, data) => {
  const mesgIcon = err ? ":skull:" : ":rocket:";
  let status = "unchanged";
  if (total > state.total) {
    status = `grew +${readableBTC((total - state.total) / 100000000)}`;
  } else if (total < state.total) {
    status = `reduced -${readableBTC((state.total - total) / 100000000)}`;
  }
  const mesg = `${mesgIcon} Coinbase BTC Reserves (${status}): \`${readableBTC(
    total / 100000000
  )}\` across ${addresses.length} wallets, AVG per wallet: \`${readableBTC(
    total / addresses.length / 100000000
  )}\`, largest = ${biggest.addr}: \`${readableBTC(
    biggest.balance / 100000000
  )}\`, smallest = ${smallest.addr}: \`${readableBTC(
    smallest.balance / 100000000
  )}\`, \`${
    badwallets.length
      ? `, ${badwallets.length} new wallets ${badwallets.join(
          ", "
        )} have reached zero!`
      : ""
  }`;
  if (
    state.biggest.addr !== biggest.addr ||
    state.biggest.balance !== biggest.balance ||
    state.smallest.addr !== smallest.addr ||
    state.smallest.balance !== smallest.balance ||
    state.total !== total
  ) {
    state.biggest = biggest;
    state.smallest = smallest;
    state.total = total;
    fs.writeFileSync("./state.json", JSON.stringify(state));
    // CHANGED
    await slack({
      text: mesg,
    });

    badwallets.forEach((b) => {
      walletList = walletList.replace(`${b}\n`, "");
      deadList = `${b}\n${deadList}`;
    });

    fs.writeFileSync("./lib/dead.txt", deadList);
    fs.writeFileSync("./lib/wallets.txt", walletList);
  }
  console.log(mesg);

  if (badwallets.length) process.exit(1);
});
